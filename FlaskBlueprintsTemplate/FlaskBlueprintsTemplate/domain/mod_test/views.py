
from FlaskBlueprintsTemplate.framework.sql import sql_controller as sql
from datetime import datetime
from flask import render_template, Blueprint
test_module = Blueprint('test_module', __name__, template_folder='templates')

@test_module.route('/')
@test_module.route('/home')
def home():
    templateData = {
        'test': sql.test()
        }
    return render_template('index.html',**templateData)

@test_module.route('/contact')
def contact():
    """Renders the contact page."""
    return render_template(
        'contact.html',
        title='Contact',
        year=datetime.now().year,
        message='Your contact page.'
    )

@test_module.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        message='Your application description page.'
    )
