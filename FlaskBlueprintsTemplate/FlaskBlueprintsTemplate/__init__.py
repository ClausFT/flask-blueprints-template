from flask import Flask
app = Flask(__name__)

from FlaskBlueprintsTemplate.domain.mod_test.views import test_module

# Register blueprints
app.register_blueprint(test_module)